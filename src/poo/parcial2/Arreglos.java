/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poo.parcial2;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;
import javax.swing.JOptionPane;

/**
 *
 * @author Mauro
 */
public class Arreglos {
    //RESTRICCIONES
    //SE USO SET, PARA PODER EXCLUIR LOS OBJETOS DUPLICADOS
    //PARA EL CASO DE EMPLEADOS, NO SE PUEDEN CARGAR DOS EMPLEADOS CON EL MISMO DNI
    //PARA EL CASO DE PROYECTOS NO SE PUEDE CARGAR UN PROYECTO CON EL MISMO ID Y NOMBRE
    //PARA EL CASO DE ASIGNACIONES 
    //      NO SE PUEDE CARGAR A UN EMPLEADO EN UN PROYECTO QUE NO EXISTE
    //      NO SE PUEDE CARGAR A UN EMPLEADO QUE NO EXISTA EN UN PROYECTO
    //      NO PERMITE QUE SE CARGUE DOS VECES A UN EMPLEADO EN UN MISMO PROYECTO
    //      SI UN EMPLEADO PUEDE PERTENECER A MAS DE UN PROYECTO
    
    Set<Asignacion> ListaAsig = new HashSet<>();
    Set<Proyecto> ListaProy = new HashSet<>();
    Empleado emp = new Empleado();
    Scanner dato = new Scanner(System.in);
    private Set<Empleado> ListaEmp = new HashSet<>();
            
    
    public void agregarEmpleado() {
        boolean ban=true;
        int aux = ListaEmp.size();
        Empleado emple = new Empleado();
        emple.setDni(Integer.parseInt(JOptionPane.showInputDialog(null, "Ingrese el DNI", "DNI", 3)));
        while (ban){
            emple.setAyn(JOptionPane.showInputDialog(null, "Ingrese Apellido y Nombre", "APELLIDO", 3));
            ban=false;
            if (emple.getAyn().isEmpty()){
                JOptionPane.showMessageDialog(null,"El campo no puede estar vacio!!");
                ban=true;
            }
        }
        while (!ban){
            emple.setTelefono(JOptionPane.showInputDialog(null, "Ingrese el telefono", "TELEFONO", 3));
            ban=true;
            if (emple.getTelefono().isEmpty()){
                JOptionPane.showMessageDialog(null,"El campo no puede estar vacio!!");
                ban=false;
            }
        }
        while (ban){
            emple.setEmail(JOptionPane.showInputDialog(null, "Ingrese el email", "EMAIL", 3));
            ban=false;
            if (emple.getEmail().isEmpty()){
                JOptionPane.showMessageDialog(null,"El campo no puede estar vacio!!");
                ban=true;
            }
        }       
        ListaEmp.add(emple);
        if (aux==ListaEmp.size()){
            JOptionPane.showMessageDialog(null,"El Empleado ya existe!!");
        }else{
            JOptionPane.showMessageDialog(null,"Empleado agregado con exito!!");
        }              
    }
    
    void eliminarEmpleado() {
        int auxdni = Integer.parseInt(JOptionPane.showInputDialog(null, "Ingrese el DNI", "ELIMINAR", 3));
        Empleado emp = null;
        for (Empleado e: ListaEmp){
            if (auxdni==e.getDni()){
                emp = e;
            }
        }
        if (ListaEmp.contains(emp)){
            ListaEmp.remove(emp);
            JOptionPane.showMessageDialog(null,"El Empleado fue eliminado");
        }else{
            JOptionPane.showMessageDialog(null,"Empleado no encontrado");            
        } 
    }

    void mostrarEmpleado() {
        System.out.println("\t----------------------------------------------------------");
        System.out.println("\t"+ "DNI+"+"\t"+"APELLIDO Y NOMBRE "+"\t"+"TELEFONO"+"\t"+"EMAIL");
        System.out.println("\t----------------------------------------------------------");
        for (Empleado e: ListaEmp){
            System.out.println("\t "+e.getDni()+"\t"+e.getAyn()+"\t\t\t"+e.getTelefono()+"\t\t"+e.getEmail());
        
        }
        System.out.println("\t----------------------------------------------------------");
        System.out.println("\tEl total de Empleados es: "+ListaEmp.size());              
    }
    
    public boolean buscarEmpleado(int auxdni) {
        boolean bande;
        Empleado emp = null;
        for (Empleado e: ListaEmp){
            if (auxdni==e.getDni()){
                emp=e;
            }
        }
        if (ListaEmp.contains(emp)){
            bande=true;
        }else{
            bande=false;
        }
        return bande;
    }
    
    
    public void agregarProyecto() {
        boolean ban=true;
        int aux = ListaProy.size();
        Proyecto proy = new Proyecto();
        proy.setId(Integer.parseInt(JOptionPane.showInputDialog(null, "Ingrese el id", "ID", 3)));
        while (ban){
            proy.setNombre(JOptionPane.showInputDialog(null, "Ingrese Nombre", "NOMBRE", 3));
            ban=false;
            if (proy.getNombre().isEmpty()){
                JOptionPane.showMessageDialog(null,"El campo no puede estar vacio!!");
                ban=true;
            }
        } 
        while (!ban){
            proy.setMonto(Integer.parseInt(JOptionPane.showInputDialog(null, "Ingrese el monto", "MONTO", 3)));
            if (proy.getMonto()>0){                
                ban=true;
            }else{
                JOptionPane.showMessageDialog(null,"El monto del Proyecto debe ser mayor a 0");
            }
        }           
        ListaProy.add(proy);
        if (aux==ListaProy.size()){
            JOptionPane.showMessageDialog(null,"El Proyecto ya existe!!");
        }else{
            JOptionPane.showMessageDialog(null,"Proyecto agregado con exito!!");
        }              
    }
    
    void eliminarProyecto() {
        int auxid = Integer.parseInt(JOptionPane.showInputDialog(null, "Ingrese el ID de Proyecto", "ELIMINAR", 3));
        Proyecto pro = null;
        for (Proyecto p: ListaProy){
            if (auxid==p.getId()){
                pro = p;
            }
        }
        if (ListaProy.contains(pro)){
            ListaProy.remove(pro);
            JOptionPane.showMessageDialog(null,"El Proyecto fue eliminado");
        }else{
            JOptionPane.showMessageDialog(null,"Proyecto no encontrado");            
        } 
    }

    void mostrarProyecto() {
        System.out.println("\t"+ "ID PROYECTO"+"\t"+"NOMBRE EMPLEADO"+"\t"+"MONTO");
        System.out.println("\t----------------------------------------------------------");
        for (Proyecto p: ListaProy){
            System.out.println("\t "+p.getId()+"\t\t\t"+p.getNombre()+"\t\t"+p.getMonto());
            //e.mostrarEmpleado();
        }
        System.out.println("\t---------------------------------------------------------");
        System.out.println("El total de Proyectos es: "+ListaProy.size());
        System.out.println("\t---------------------------------------------------------");
        
        
    }

    void agregarEmpleadoProyecto() {
        int aux = ListaAsig.size();
        Asignacion asig = new Asignacion();
        int auxdni=Integer.parseInt(JOptionPane.showInputDialog(null,"Ingrese el DNI del empleado", "DNI",3));
        int auxid =Integer.parseInt(JOptionPane.showInputDialog(null, "Ingrese el id de proyecto", "ID", 3));
        boolean ban1 = buscarEmpleado(auxdni);
        boolean ban2 = buscarProyecto(auxid);
        if (ban1 && ban2){
            asig.setDniEmpleado(auxdni);
            asig.setIdProyecto(auxid);
            ListaAsig.add(asig);
            if (aux==ListaAsig.size()){
                JOptionPane.showMessageDialog(null,"El Empleado ya esta en el Proyecto");
            }else{
                JOptionPane.showMessageDialog(null,"Empleado agregado al Proyecto con exito!!");
            } 
        }else{
            if (!ban1){
                JOptionPane.showMessageDialog(null,"El Empleado no existe");
            }else{
                JOptionPane.showMessageDialog(null,"El Proyecto no existe");
            }
        }        
    }

    void eliminarEmpleadoProyecto() {
        int auxid = Integer.parseInt(JOptionPane.showInputDialog(null, "Ingrese el Id de Proyecto", "ELIMINAR", 3));
        int auxdni = Integer.parseInt(JOptionPane.showInputDialog(null, "Ingrese el DNI del Empleado", "ELIMINAR", 3));
        Asignacion asig = null;
        for (Asignacion a: ListaAsig){
            if (auxid==a.getIdProyecto() && auxdni==a.getDniEmpleado()){
                asig = a;
            }
        }
        if (ListaAsig.contains(asig)){ 
            ListaAsig.remove(asig);
            JOptionPane.showMessageDialog(null,"El Empleado fue eliminado del Proyecto");
        }else{
            JOptionPane.showMessageDialog(null,"el Empleado no pertenece al Proyecto");            
        } 
    }

    void listarDatosProyecto() {
        int cant = 0;
        int auxid = Integer.parseInt(JOptionPane.showInputDialog(null, "Ingrese el Id de Proyecto", "LISTAR", 3));
        System.out.println("\t-------------------------------------------------------------");
        System.out.println("\t"+ "ID PROYECTO"+"\t\t"+"DNI EMPLEADO"+"\t\t"+"NOMBRE EMPLEADO"); 
        System.out.println("\t-------------------------------------------------------------");
        for(Asignacion a: ListaAsig){
            if(auxid==a.getIdProyecto()){
                System.out.println("\t "+a.getIdProyecto()+"\t\t\t"+a.getDniEmpleado()+"\t"+buscarNombreEmpleado(a.getDniEmpleado()));
                cant++;
            }
        }
        System.out.println("\t--------------------------------------------------------------");
        System.out.println("\tEl total de Empleados en el Proyecto es: "+cant);
    }

    void calcularMonto() {
        int monto=0;
        for(Proyecto p: ListaProy){
            monto = monto + p.getMonto();
        }
        JOptionPane.showMessageDialog(null,"El monto total de Proyectos es "+ monto);
        //System.out.println("el monto total es " + monto);
    }

    private boolean buscarProyecto(int auxid) {
        boolean bande=false;
        for (Proyecto p: ListaProy){
            if (auxid==p.getId()){
                bande=true;
            }
        }        
        return bande;
    }

    private String buscarNombreEmpleado(int dniEmpleado) {
        String nombre="";
        for (Empleado e: ListaEmp){
            if (dniEmpleado==e.getDni()){
                nombre=e.getAyn();
            }
        }
        return nombre;
    }
    
}
