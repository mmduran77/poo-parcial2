/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poo.parcial2;

/**
 *
 * @author Mauro
 */
public class Asignacion {
    private int idProyecto;
    private int dniEmpleado;

    public Asignacion() {
    }

    public Asignacion(int idProyecto, int dniEmpleado) {
        this.idProyecto = idProyecto;
        this.dniEmpleado = dniEmpleado;
    }

    public int getIdProyecto() {
        return idProyecto;
    }

    public void setIdProyecto(int idProyecto) {
        this.idProyecto = idProyecto;
    }

    public int getDniEmpleado() {
        return dniEmpleado;
    }

    public void setDniEmpleado(int dniEmpleado) {
        this.dniEmpleado = dniEmpleado;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 41 * hash + this.idProyecto;
        hash = 41 * hash + this.dniEmpleado;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Asignacion other = (Asignacion) obj;
        if (this.idProyecto != other.idProyecto) {
            return false;
        }
        if (this.dniEmpleado != other.dniEmpleado) {
            return false;
        }
        return true;
    }

    
    
    
}
