package poo.parcial2;

import javax.swing.JOptionPane;

/**
 *
 * @author Mauro
 */
public class POOParcial2 {

    private static int opc=0;
    static Arreglos pro = new Arreglos();

    public static void main(String[] args) {
        boolean ban=true;         
        
        while(ban){
            try{    
                opc=mostrarMenu();
                    switch (opc){
                    case 1: menuEmpleado();                                   
                            break;
                    case 2: menuProyecto();
                            break;
                    case 3: ban=false;
                            System.out.println("fin del programa");
                            break;                  
                } 
            }catch (Exception ex){
                JOptionPane.showMessageDialog(null,"Error. Intente de nuevo");
            }
        }
       
    }
    
    
    private static int mostrarMenu() {
        int op=0;
        boolean bandera=true;
        while (bandera){
            op = Integer.parseInt(JOptionPane.showInputDialog(null,
                         "1) EMPLEADO\n"
                       + "2) PROYECTO\n"
                       + "3) SALIR", "MENU DE OPCIONES", 3));
            bandera=(op>=1 && op<=3)? false : true;
        }    
        return op;
    }

    private static void menuEmpleado() {
        int op=0;        
        do{
            boolean bandera=true;
            while (bandera){
                op = Integer.parseInt(JOptionPane.showInputDialog(null,
                             "1) AGREGAR EMPLEADO\n"
                           + "2) ELIMINAR EMPLEADO\n"
                           + "3) LISTAR EMPLEADO\n"
                           + "4) VOLVER", "MENU DE EMPLEADO", 3));
                if (op>=1 && op<=4)
                    bandera= false;
            }  
            switch (op) {
                    case 1: pro.agregarEmpleado();
                            break;
                    case 2: pro.eliminarEmpleado();
                            break;
                    case 3: pro.mostrarEmpleado();                            
                            break;
                    case 4: System.out.println("Saliendo al menu principal");;
                            break;
            }
        }while (op!=4);
        
    }

    private static void menuProyecto() {
        int op=0;        
        do{
            boolean bandera=true;
            while (bandera){
                op = Integer.parseInt(JOptionPane.showInputDialog(null,
                             "1) AGREGAR PROYECTO\n"
                           + "2) ELIMINAR PROYECTO\n"
                           + "3) LISTAR PROYECTO\n"
                           + "4) AGREGAR EMPLEADO A UN PROYECTO\n"
                           + "5) ELIMINAR EMPLEADO A UN PROYECTO\n"
                           + "6) LISTAR DATOS DE PROYECTO\n"
                           + "7) CALCULAR MONTO PROYECTO\n"          
                           + "8) VOLVER", "MENU DE EMPLEADO", 3));
                if (op>=1 && op<=8)
                    bandera= false;
            }  
            switch (op) {
                    case 1: pro.agregarProyecto();
                            break;
                    case 2: pro.eliminarProyecto();
                            break;
                    case 3: pro.mostrarProyecto();
                            break;
                    case 4: pro.agregarEmpleadoProyecto();
                            break;
                    case 5: pro.eliminarEmpleadoProyecto();
                            break;
                    case 6: pro.listarDatosProyecto();
                            break;
                    case 7: pro.calcularMonto();
                            break;
                    case 8: System.out.println("Saliendo al menu principal");;
                            break;

            }
        }while (op!=8);
        
    }
}
