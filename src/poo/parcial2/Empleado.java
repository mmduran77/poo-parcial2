package poo.parcial2;

/**
 *
 * @author Mauro
 */
public class Empleado {
    private int dni;
    private String ayn;
    private String telefono;
    private String email;

    public Empleado() {
    }

    public Empleado(int dni, String ayn, String telefono, String email) {
        this.dni = dni;
        this.ayn = ayn;
        this.telefono = telefono;
        this.email = email;
    }

    public int getDni() {
        return dni;
    }

    public void setDni(int dni) {
        this.dni = dni;
    }

    public String getAyn() {
        return ayn;
    }

    public void setAyn(String ayn) {
        this.ayn = ayn;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + this.dni;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Empleado other = (Empleado) obj;
        if (this.dni != other.dni) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Empleado{" + "dni=" + dni + ", ayn=" + ayn + ", telefono=" + telefono + ", email=" + email + '}';
    }

    void mostrarEmpleado() {
        System.out.println(getDni()+" "+ getAyn()+ " " + getTelefono()+ " " +getEmail());
    }

    
    
    
    
}
